#!/bin/bash
set -e
set -x
sed "s|PWD|`pwd`|" auto_etc_hosts.service.sample > auto_etc_hosts.service
sudo cp auto_etc_hosts.service /etc/systemd/system
sudo systemctl daemon-reload
sudo systemctl enable auto_etc_hosts.service
sudo systemctl start auto_etc_hosts.service
