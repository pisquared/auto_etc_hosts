# Auto /etc/hosts block

This runs a service that adds entries to `/etc/hosts` file which blocks sites listed in `sites.txt`.

If the user comments or manually unblocks one of the listed sites, the service will recover the blocked sites after maximum of seconds defined in the `sleep` file
