#!/bin/bash

while read p; do
  sed -i "s/^#[[:space:]]*127.0.0.1\t${p}$/127.0.0.1\t${p}/" /etc/hosts 
  grep -q -P "^127.0.0.1\t${p}$" /etc/hosts || bash -c "echo -e '127.0.0.1\t${p}' >> /etc/hosts" 
done <sites.txt
