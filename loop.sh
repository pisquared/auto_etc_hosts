#!/bin/bash

while true; do
  git checkout -f master
  git fetch origin
  git checkout -- .
  git reset --hard origin/master
  ./auto_etc_hosts.sh
  SLEEP=`cat sleep`
  sleep "$SLEEP"
done
